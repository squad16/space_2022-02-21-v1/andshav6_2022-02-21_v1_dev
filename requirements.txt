click
Sphinx
coverage
flake8
python-dotenv>=0.5.1
pyspark==2.4.4
pytest